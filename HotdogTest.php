<?php

require_once "unit.php";

class HotdogTest extends PHPUnit_Framework_TestCase
{
	public function testMayoAvailable()
	{

		$myHotdog = new Hotdog();

		$this->assertEquals("Available", $myHotdog->getMayo());
	}

	public function testMustardAvailable()
	{
		$myHotdog = new Hotdog();

		$this->assertEquals("Not Available", $myHotdog->getMustard());

	}
}

$suite = new PHPUnit_Framework_TestSuite('HotdogTest');
PHPUnit_TextUI_TestRunner::run($suite);

?>