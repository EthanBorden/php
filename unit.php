<?php


class Hotdog


 {
 	private $condiments;

 	public function __construct()
 	{
 		$this->condiments["mayo"] = true;
 		$this->condiments["mustard"] = false;
 		$this->condiments["ketchup"] = true;
 	}

 	public function getMayo()
 	{

 		if ($this->condiments["mayo"]) 
 		{
 			echo "Available";
 		}
 		else
 		{
 			echo "Not Available";
 		}
 	}

 	public function getMustard()
 	{
 		if ($this->condiments["mustard"])
 		{
 			echo "Available";

 		}
 		else
 		{
 			echo "Not Available";
 		}

 	}
 }